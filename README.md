This tool is an example of a rust tool for the Toolforge
environment using the build service.

See more details here:
https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service/My_first_Buildpack_Rust_tool
